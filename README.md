# Cloud and infrastructure as Service Basics 

## Table of Contents
- [Project Description](#project-description)
- [Installation](#installation)
- [Usage](#usage)
- [Screenshots](#screenshots)
- [How to Contribute](#how-to-contribute)
- [Tests](#test)
- [Questions](#questions)
- [References](#references)
- [License](#license)

## Project Description

Creating a server on DigitalOcean and deploying an application on the server. 

* Setup and configured a server on DigitalOcean 
* Created and configured a new Linux user on the Droplet (Security best practice)
* Deployed and ran a Java application on Droplet  


## Installation

$ brew install nodejs 

$ sudo apt install net-tools

$ sudo apt install openjdk-8-jre-headless


## Usage 

$ java -jar java-react-example.jar & 

## Screenshots 

![Adding ssh](/images/adding_sshkey.png)
> ssh key addition on DigitalOcean 

![Checking Processes for application](/images/checking_if_application_is_runnung.png)
> checking processes if application is running 

![Connected to Cloud server](/images/connected_to_droplet.png)
> connection to cloud server by root 

![Creating Server](/images/creating_server.png)
> creating server on DigitalOcean 

![Enabling ssh Connection](/images/enabling_ssh_connection_to_ip.png)
> enabling ip address access to port 22 

![Installing Net Tools](/images/installing_net_tools.png)
> installation command for net-tools 

![Java Application](/images/java_application_built_using_gredle.png)
> Java Application built using Gradle  

![Moving Application to Server](/images/moving_application_from_repository_to_cloud_server.png)
> Moving Java application to cloud server 

![Opening Port 7071](/images/opening_port7071_to_access_from_browser.png)
> Opening port 7071 

![Removing Application Repo](/images/removing_repo.png)
>Removing cloned repo 

![Running Application on Cloud Server](/images/running_java_application.png)
> Application Running on Cloud Server 

![Active Servers](/images/active_servers.png)
>Active Servers on Cloud server

![Connecting to Cloud server Through ssh](/images/ssh_to_droplet.png)
> ssh to cloud server 

![UI of Application](/images/UI_of_application.png)
> UI of Application Displayed on Browser

![Adding User to sudo Group](/images/user_creation/adding_user_to_sudo_group.png)
> Adding User to sudo Group 

![Creating ssh Folder](/images/user_creation/cloud_sshfolder.png)
> ssh folder creation 

![Creating User](/images/user_creation/creating_user.png)
> User creation  

![Authorized keys](/images/user_creation/authorized_keys.png)
> Putting public ssh key in cloud server

![Display ssh Public Key](/images/user_creation/ssh_publickey.png)
> Command to display ssh key 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/cloud-and-infrastructure-as-service-basics.git

2. Create new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review 

## Tests

Test were run using ./gradlew test 

## Questions

feel free to contact me for further questions via:

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com 
## References

https://gitlab.com/omacodes98/cloud-and-infrastructure-as-service-basics

## License

The MIT License 

for more information you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji. 